const database = require('../_helpers/db');
const heatmap = database.Heatmap;
const moment = require('moment');
const { request } = require('express');
const hcache = require('../heatmapCache/hcache.service');
const { Footfall } = require('../_helpers/db');
const { set } = require('mongoose');

module.exports = {
    getAll,
    getById,
    create,
    update,
    remove,
    truncate,
    getHeatMap,
    formatObj
    
   
};

/***********************************************************************************
To get all records from requested database table
function return array of object from collection  
************************************************************************************/
async function getAll(request) {
    let query = request.query;
 
    if(query.getall==="true"){
        
        return await heatmap.find().exec();
    }


    let options = {
        limit: parseInt(query.limit>0? query.limit:10),
        page: parseInt(query.page>0? query.page:1),
    }
    delete query['limit'];
    delete query['page'];

        return await heatmap.paginate(query,options);

}

  





/************************************************************************************ 
To get single record by id (primary key).
Function takes one paramter (id) and returns a single object or empty array if not found
*************************************************************************************/
async function getById(id) {
    try {
       let result= await heatmap.findById(id);
       if(!result) throw "Not_found"
    } catch (e) {
        throw "Not_found";
    }
}

async function getHeatMap(request,token){
  
    return await hcache.getpoints(request,token.sub);
   
}

/* ***********************************************************************************
To create a new record
Function takes one parameter (request_params) 
request_param = object with all required detail 
It returns saved object with its new id (Primary key)
**************************************************************************************/
async function create(request_params,token) {
   
    var returnArr = [];
    var updated = [];
    for (var i = 0; i < request_params.length; i++) {
        if(request_params[i].det.length==0){
            request_params[i].P = 1;       
        }else{
            request_params[i].sid = token.sub;
            const detail = new heatmap(request_params[i]);
            returnArr[i] = await detail.save();
        }     
    }
    console.log(returnArr.length,"Saved to db")
    return returnArr.length;
}

async function formatObj(){
    let objects = await heatmap.find({ P: 0, sid: 'rsd' }, { 'det': 1, T: 1 }).sort({ T: -1 }).limit(5000);
    if (objects.length > 0) {
      
        let detObjects = [];
        let newtime = "";
        let det = objects.filter(async e => {
            newtime = e.T;
            e.det.filter(async c => detObjects.push(c))
        })

        newtime = newtime.toISOString();
        let splited = newtime.split('T');
        let T = splited[0];
        let newObj = { T, "sid": "rsd", "det": detObjects }

        let isSaved = await hcache.saveDetail(newObj);
        console.log("IsSaved", isSaved);

        if (isSaved) {

            let res = [];
            for (let j = 0; j < objects.length; j++) {
                res[j] = await heatmap.findByIdAndUpdate(objects[j]._id, { P: 1 })
            }
            console.log("status updated", res.length);
            return res.length;

        } else {
            console.log("status not updated");

            return false
        }
    } else {
        console.log("no object ready for cache")
        return "no new objects";
    }

}



/* ***********************************************************************************
To update existing record
Function takes two parameters 
1: id (id) which needs to be updated. 
2: Updated Object (request_params).
It returns updated object of requested id. 
***************************************************************************************/
async function update(id, request_params) {
    //check if record existed in database which need to be updated
    const existing_object = await heatmap.findById(id);
    if (!existing_object) throw 'Not_found';

    Object.assign(existing_object, request_params);
    return await existing_object.save();

}

 

/**************************************************************************************
To delete record from database
Function takes one Parameter (id), id which needs to be removed/deleted.
 ***************************************************************************************/
async function remove(id) {
    try{
        await heatmap.findByIdAndRemove(id);
    }catch(e){
        throw "Not_found"
    }
}

async function truncate(request) {
    try {
        let result = await heatmap.remove({});
        return result;
    } catch (e) {
        throw "Not_found"
    }
}







