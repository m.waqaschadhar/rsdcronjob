const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//Schema to store in mongo database

const schema = new Schema({
    cid:{type:String,required:false},
    sid:{type:String,required:false},
    F:{type:String,required:false},
    det: [{
        _id:false,
        x:{type:Number,required:false},
        y:{type:Number,required:false},
        v:{ type:Number,required:false,default:1},
    }],
    T:{type:Date, required:false},
    P:{type:Number,required:false,default:0}
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('mb_heatmap', schema);