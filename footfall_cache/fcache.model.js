const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//Schema to store in mongo database

const schema = new Schema({
    sid:{type:String,required:false},
    T:{ type: Date, required:false },
    F:{type:String,required:false,default:"0"},
    detail: [{
        _id:false,
        h: {type:String,required:false},
        total:{type:Number,required:false},
        male: {type:Number,required:false},
        female: {type:Number,required:false},
        mask: {type:Number,required:false},
        noMask: {type:Number,required:false},
        kid: {type:Number,required:false},
        teen: {type:Number,required:false},
        young: {type:Number,required:false},
        adult: {type:Number,required:false},
    }]
},{versionKey:false}
);

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('mb_fcache', schema);