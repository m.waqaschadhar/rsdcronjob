const express = require('express');
const router = express.Router();
const fcache_service = require('./fcache.service');
const  responses = require('../_helpers/response_handler')


//Controller handle incomming requests, request method and url. Then route it to particular function which handling further processing   
router.delete('/delete/table', truncate);

module.exports = router;

function truncate(req, res, next) {
    fcache_service.truncate(req)
        .then(response => responses.deleteSuccess(res,response))
        .catch(err => next(err));
}
