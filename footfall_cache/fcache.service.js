const database = require('../_helpers/db');
const fcache = database.Fcache;
const moment = require('moment');
const { months } = require('moment');

module.exports = {
   truncate,
    makeFootfall
};

async function makeFootfall(){

    let time = moment().startOf('day').add(5,'hours');
    let hour= new Date(moment().add(5,'hours') ).getHours().toString();
   if(hour.length==1){

       hour = "0"+hour;
   }
    let object= {
        sid:"rsd",
        T:time,
        F:"0",
        detail: [{
            h: "00",
            total:10,
            male: 4,
            female:6,
            mask: 7,
            noMask: 3,
            young: 2,
            adult: 1,
        }]
    }
   
    let existing = await fcache.findOne({ T: time, F:"0", sid: "rsd" });
    if(existing){
    let updatedObject = await getRandomObject(hour,time);
    let updatedHour = updatedObject.detail[0];
    let hours = existing.detail;
    var result = hours.find(a => a.h === hour);
    if (result == undefined) {
        existing.detail.push(updatedHour);
    } else {
        existing.detail.map(a => {
            if (a.h == updatedHour.h) {
                a.total += updatedHour.total;
                a.male += updatedHour.male;
                a.female += updatedHour.female;
                a.mask += updatedHour.mask;
                a.noMask += updatedHour.noMask;
                a.young += updatedHour.young;
                a.adult += updatedHour.adult;
            }
            return a;
        })
    }
    if (existing.__v) {
        delete existing.__v;
    }
    return await existing.save()

  }else{
    let saveDetail = new fcache(object);
    return await saveDetail.save()
  }
    
}

async function getRandomObject(hour,time){
    let arr = [0,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,15,12,9,5,2,0]
    let random = Math.round( arr[parseInt(hour)]*(Math.random()*10+1));
    let nomask = Math.abs(Math.floor((10/100)*random));
    let object= {
        sid:"rsd",
        T:time,
        F:"0",
        detail: [{
            h:hour,
            total: Math.round(random),
            male: Math.abs( Math.floor((40/100)*random)),
            female:Math.abs( Math.floor((60/100)*random)),
            mask: Math.abs(Math.floor((90/100)*random)),
            noMask: Math.abs(Math.floor((10/100)*random)),
            young: Math.abs(Math.floor((80/100)*nomask)),
            adult: Math.abs(Math.floor((20/100)*nomask)),
        }]
    }
return object;

}

async function truncate(){

    try {
        let result = await fcache.remove({});
        return result;
    } catch (e) {
        throw "Not_found"
    }
}





