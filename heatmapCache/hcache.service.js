const database = require('../_helpers/db');
const hcache = database.Hcache;
const moment = require('moment')

module.exports = {
    getAll,
    getById,
    create,
    getpoints,
    update,
    remove,
    truncate,
    saveDetail,
    getAnalytics,
    saveSaphireDetail
};

/***********************************************************************************
To get all records from requested database table
function return array of object from collection  
************************************************************************************/
async function getAll(request) {

    let query = request.query;
    if(query.getall==="true"){
        
            return await hcache.find().exec();
        }
    
    let options = {
        limit: parseInt(query.limit>0? query.limit:10),
        page: parseInt(query.page>0? query.page:1)
       
    }
    delete query['limit'];
    delete query['page'];
    delete query['getall'];

        return await hcache.paginate(query,options);

}

async function getpoints(request,sub){
      
    request.sid = sub;
    if(request.start && request.end){
        request.start = moment(request.start).add(5,'hours').format();
        request.end = moment(request.end).add(5,'hours').format();
      
        const date1 = new Date(request.start.split('T')[0]);
        const date2 = new Date(request.end.split('T')[0]);
            return await hcache.aggregate([
                {
                    $match: {
                        $and: [{ sid: request.sid },
                        { T: { $gte: date1, $lte: date2 } },
                        ]
                    }
                },
                { $unwind: '$det' },
                {
                    $group: {
                        "_id": { x: '$det.x', y: '$det.y' },
                        "v": { $sum: '$det.v' }
                    }
                },
                {
                    $project: {
                        "x": '$_id.x', 
                        "y":  '$_id.y',
                        "v": '$v',
                        '_id': 0
                    }
    
    }

             ])
    
    }else{
    let start = moment(new Date()).startOf('day')
    let end = moment(new Date()).endOf('day')
    return await hcache.aggregate([
        { $match: {
         $and:[  {sid: request.sid},
             { T:{ $gte:  moment(start ).toDate() , $lte: moment(end).toDate() }},
             ]
        }  },
        
        { $unwind:'$det' },
        { $group: {
            "_id": {x: '$det.x', y:'$det.y' },
             "v": { $sum: '$det.v' }
            }
        },
        { $project:{
            "x": '$_id.x',
            "y": '$_id.y',
            "v": '$v',
            '_id': 0
            }
            }
         
     ])



    }


}


async function getAnalytics(request,sub){
        request.sid = sub;
    if(request.start && request.end){
        const date1 = new Date(request.start.split('T')[0]);
        const date2 = new Date(request.end.split('T')[0]);
        if(date1.getTime()===date2.getTime()){
            return await hcache.aggregate([
                { $match: {
                 $and:[  {sid: request.sid},
                     { T: date1  },
                     ]
 
                }  },
                 {$unwind: "$detail"},
                 {$group: {
                     "_id":"$detail.h",
                     "total": { $sum: "$detail.total" },
                     "male":{ $sum :"$detail.male" },
                     "female":{ $sum :"$detail.female" },
                     "mask":{ $sum :"$detail.mask" },
                     "noMask":{ $sum :"$detail.noMask" },
                     "kid":{ $sum :"$detail.kid" },
                     "teen":{ $sum :"$detail.teen" },
                     "young":{ $sum :"$detail.young" },
                     "adult":{ $sum :"$detail.adult" }
                         }}             
             ])
 
        }else{
            return await hcache.aggregate([
                { $match: {
                 $and:[  {sid: request.sid},
                     { T:{ $gte:  date1 , $lte: date2 }},
                     ]
 
                }  },
                 {$unwind: "$detail"},
                 { $project: { "date" : { $dateToString: { format: "%Y-%m-%d", date: "$T" }} }},
                 {$group: {
                     "_id":"$date",
                     "total": { $sum: "$detail.total" },
                     "male":{ $sum :"$detail.male" },
                     "female":{ $sum :"$detail.female" },
                     "mask":{ $sum :"$detail.mask" },
                     "noMask":{ $sum :"$detail.noMask" },
                     "kid":{ $sum :"$detail.kid" },
                     "teen":{ $sum :"$detail.teen" },
                     "young":{ $sum :"$detail.young" },
                     "adult":{ $sum :"$detail.adult" }
                         }}
                 
                 
             ])
 
          
        }
      
        
     
        
    
    }else{
    let start = moment(new Date()).startOf('day')
    let end = moment(new Date()).endOf('day')
    return await hcache.aggregate([
        { $match: {
         $and:[  {sid: request.sid},
             { T:{ $gte:  moment(start ).toDate() , $lte: moment(end).toDate() }},
             ]

        }  },
         {$unwind: "$detail"},
         {$group: {
             "_id":"$detail.h",
             "total": { $sum: "$detail.total" },
             "male":{ $sum :"$detail.male" },
             "female":{ $sum :"$detail.female" },
             "mask":{ $sum :"$detail.mask" },
             "noMask":{ $sum :"$detail.noMask" },
             "kid":{ $sum :"$detail.kid" },
             "teen":{ $sum :"$detail.teen" },
             "young":{ $sum :"$detail.young" },
             "adult":{ $sum :"$detail.adult" }
                 }}
         
         
     ])



    }

}

/************************************************************************************ 
To get single record by id (primary key).
Function takes one paramter (id) and returns a single object or empty array if not found
*************************************************************************************/
async function getById(id) {
    try{    
        return await hcache.findById(id);
    }catch(e){
        throw "Not_found"
    }
}

async function getHeatmap(){

}
/* ***********************************************************************************
To create a new record
Function takes one parameter (request_params) 
request_param = object with all required detail 
It returns saved object with its new id (Primary key)
**************************************************************************************/
async function create(request_params) {
    //Check if record existed already
    const existing_record = await hcache.findOne({ 
        mac_address: request_params.mac_address,
    });
    if (existing_record) {
        return {status:false,message:"Already_existed",data:existing_record};
    }
    //Saving to databse
    const detail = new hcache(request_params);
    const res= await detail.save();
    return { status:true,message:"inserted",data:res }
    
}

async function saveDetail(request){
    let det = request.det.filter(e=> e.x <=1000 && e.y <= 900 );
    
    let existing = await hcache.findOne({ T: request.T, sid: request.sid });
    
    if (existing) {
        let found = []
        for (let i = 0; i < det.length; i++) {
            found = [];
            existing.det.map(a => {
                if (a.x == det[i].x && a.y == det[i].y) {
                    a.v += 1;
                    found.push(det[i])
                } return a;
            })
            if (found.length == 0) {
                existing.det.push(det[i]);
            }
        }
     
        console.log("existing det",existing.det.length)
      let updated =   await existing.save()
     if(updated._id){
         return true;
     } else { return false }
    } else {

            request.det = await getValues(det);
        
        let saveDetail = new hcache(request);
        let saved = await saveDetail.save();
        if(saved._id){
            return true;
        }else{ return false }
    }

}

async function saveSaphireDetail(request){
    let det = request.det.filter(e=> e.x <=750 && e.y <= 750 );
    
    let existing = await hcache.findOne({ F:request.F, cid:request.cid, T: request.T, sid: request.sid });
    
    if (existing) {
        let found = []
        for (let i = 0; i < det.length; i++) {
            found = [];
            existing.det.map(a => {
                if (a.x == det[i].x && a.y == det[i].y) {
                    a.v += 1;
                    found.push(det[i])
                } return a;
            })
            if (found.length == 0) {
                existing.det.push(det[i]);
            }
        }
     
        console.log("existing det",existing.det.length)
      let updated =   await existing.save()
     if(updated._id){
         return true;
     } else { return false }
    } else {

            request.det = await getValues(det);
        
        let saveDetail = new hcache(request);
        let saved = await saveDetail.save();
        if(saved._id){
            return true;
        }else{ return false }
    }

}


async function getValues(inputArray){

    var result = Object.values(inputArray.reduce(function(r, e) {
        var key = e.x + '|' + e.y;
        if (!r[key]) r[key] = e;
        else {
          r[key].v += e.v;
        
        }
        return r;
      }, {}))
    
      return result

}
/* ***********************************************************************************
To update existing record
Function takes two parameters 
1: id (id) which needs to be updated. 
2: Updated Object (request_params).
It returns updated object of requested id. 
***************************************************************************************/
async function update(id, request_params) {
    //check if record existed in database which need to be updated
    const existing_object = await hcache.findById(id);
    if (!existing_object) throw 'Not_found';

    Object.assign(existing_object, request_params);
    return await existing_object.save();

}

/**************************************************************************************
To delete record from database
Function takes one Parameter (id), id which needs to be removed/deleted.
 ***************************************************************************************/
async function remove(id) {
    try{
    await hcache.findByIdAndRemove(id);
    }catch(e){
        throw "Not_found"
    }
}
async function truncate(request) {
    try{
     let result=  await  hcache.remove({});
     return result;
    }catch(e){
        throw "Not_found"
    }
}






