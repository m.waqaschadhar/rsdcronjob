const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//Schema to store in mongo database

const schema = new Schema({
    sid:{type:String,required:false},
    cid:{type:String,required:false},
    T:{ type: Date, required:false },
    det: [{
        _id:false,
        x: {type:Number,required:false},
        y: { type:Number,required:false },
        v: { type:Number,required:false,default:1 }
    }]
},{versionKey:false}
);

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('mb_hcache', schema);