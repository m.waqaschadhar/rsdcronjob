const expressJwt = require('express-jwt');
const config = require('config.json');
const jwt = require('jsonwebtoken');


module.exports = {
    validateToken: (req, res, next) => {
      const authorizationHeaader = req.headers.authorization;
      let result;
       const excluded = ['/users/authenticate', '/uploads/flag' , '/users/register', '/users/forgot','/media','/media/profileImage'];
     
       if (excluded.indexOf(req.url) > -1) return next();
      if (authorizationHeaader) {
        const token = req.headers.authorization.split(' ')[1]; // Bearer <token>
        const options = {
          expiresIn: '360d',
          };
        try {
          // verify makes sure that the token hasn't expired and has been issued by us
          result = jwt.verify(token, config.secret);
  
          // Let's pass back the decoded token to the request object
          req.decoded = result;
         
          // We call next to pass execution to the subsequent middleware
          next();
        } catch (err) {
          // Throw an error just in case anything goes wrong with verification
          throw new Error(err);
        }
      } else {
        result = { 
          error: `Authentication_error`,
          status: 401
        };
        res.status(401).send(result); 
      }
    }
  };
