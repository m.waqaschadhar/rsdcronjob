module.exports= {
    successResponse,
    errorResponse,
    insertSuccess,
    updateSuccess,
    deleteSuccess,
    getSuccess
};

function successResponse(res,response){
    return  res.json({
        status:true,
        message: "inserted",
        data:response
    });
}

function insertSuccess(res,response){
    return  res.json({
        status:true,
        message: "inserted",
        data:response
    });
}
function updateSuccess(res,response){
    return  res.json({
        status:true,
        message: "updated",
        data:response
    });
}
function  deleteSuccess(res,response){
    return  res.json({
        status:true,
        message: "deleted",
        data:response
    });
}
function  getSuccess(res,response){
    return  res.json({
        status:true,
        message: "fetched",
        data:response
    });
}


function errorResponse(next,res,err){
    return  res.json({
        status:false,
        message: err,
        data:null
    });

}

