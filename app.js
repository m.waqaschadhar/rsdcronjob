require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const {validateToken } = require('_helpers/jwt');
const {formatObj} = require('./heatmap/heatmap.service')

const errorHandler = require('_helpers/error-handler');

var io = require('socket.io-client')
var cron = require('node-cron');



// maria B cache for heatmap
cron.schedule('*/30 * * * *', async () => {
   
    await formatObj();
    
  });

 app.use(express.json({limit: '170mb'}));
 app.use(express.urlencoded({limit: '170mb',extended:false}));

var corsOptions = {
    origin: '*',
    limit:'50mb',
    optionsSuccessStatus: 200
};
app.use(cors(corsOptions));

app.use(errorHandler);





// start server
const port = 4200;//process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 4200;
const server = app.listen(port, function () {
    console.log('Rsd cache Server listening on port ' + port);
});
server.timeout = 900000;
